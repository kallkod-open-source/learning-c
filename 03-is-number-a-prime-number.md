# Task description.

Write a program to check if some number is prime or not.

You can hardcode number in the source code, so application does not need to get input from the user.
