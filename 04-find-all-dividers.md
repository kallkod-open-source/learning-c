# Task

## Stage 1
Print all dividers for some number. Each divider has to be a prime number.

## Stage 2
Put all dividers into array. Print array when all dividers are found.
