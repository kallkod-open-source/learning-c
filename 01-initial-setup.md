Install tools required for the lessons

## Windows

#### Tools, accounts, etc ...

1. Install OpenSSH client on your own machine: https://docs.microsoft.com/sv-se/windows-server/administration/openssh/openssh_install_firstuse
1. Verify if OpenSSH client works (launch cmd.exe run following command): `ssh login@38.242.239.161` <br>
Your login: 
Password: 
1. Install Visual Studio Code: https://code.visualstudio.com/download
1. Start to read the book "The C Programming Language": https://en.wikipedia.org/wiki/The_C_Programming_Language.<br>
Check if there is a translation into your language.

#### Easy connection to the server
1. Run on your local machine: `ssh-keygen`
1. Show content of public key: `type C:\users\login\.ssh\id_rsa.pub`
1. Open VS Code
1. Connect to `ssh login@38.242.239.161`<br>
Read more: https://code.visualstudio.com/docs/remote/ssh
1. Open file `~/.ssh/authorized_keys`
1. Paste public key there, save the file and close the file

#### GitLab (optional)
1. Create account on https://gitlab.com
1. Add your public key to https://gitlab.com/-/profile/keys

## Linux
1. Install development tools: `sudo apt-get install build-essential`
1. Install Visual Studio Code: https://code.visualstudio.com/download
1. Create account on https://gitlab.com
1. Run on your local machine: `ssh-keygen` in case if you don't have key yet
1. Add public key to https://gitlab.com/-/profile/keys
1. Start to read the book "The C Programming Language": https://en.wikipedia.org/wiki/The_C_Programming_Language.<br>
Check if there is translation into your language.
