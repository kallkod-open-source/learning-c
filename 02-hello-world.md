Source code:
```c
#include <stdio.h>

int main()
{
    printf("Hello world\n");
}
```

* Launch VS Code and connect to server
* Create new file, copy-paste source code there and save
* Open terminal in MS Code
* Locate your file and run command: `gcc your-file.c`
* Run `./a.out`

Explain items in the source code:
* `#include <stdio.h>`
* `int main()`
* `printf();`
* `"Hello world\n"`
